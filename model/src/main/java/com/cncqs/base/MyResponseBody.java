package com.cncqs.base;

import java.io.Serializable;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 3:49
 */
public class MyResponseBody implements Serializable {
    private static final long serialVersionUID = -1491499610244557029L;
    public static int CODE_SUCCESS = 0;
    public static int CODE_FAILURED = -1;
    public static String[] NOOP = new String[] {};

    /**
     * 处理状态：0成功
     * */
    private int code;
    /**
     * 异常信息
     * */
    private String message;
    /**
     * 返回数据
     * */
    private Object data;

    private MyResponseBody(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 处理成功，并返回数据
     *
     * @param data
     *            数据对象
     * @return data
     */
    public static final MyResponseBody success(Object data) {
        return new MyResponseBody(CODE_SUCCESS, "操作成功", data);
    }

    /**
     * 处理成功
     *
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody success() {
        return new MyResponseBody(CODE_SUCCESS, "操作成功", NOOP);
    }

    /**
     * 处理成功
     *
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody success(String message) {
        return new MyResponseBody(CODE_SUCCESS, message, NOOP);
    }

    /**
     * 处理成功
     *
     * @param message
     *            消息
     * @param data
     *            数据对象
     * @return data
     */
    public static final MyResponseBody success(String message, Object data) {
        return new MyResponseBody(CODE_SUCCESS, message, data);
    }

    /**
     * 处理失败，并返回数据（一般为错误信息）
     *
     * @param code
     *            错误代码
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody failure(int code, String message) {
        return new MyResponseBody(code, message, NOOP);
    }

    /**
     * 处理失败
     *
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody failure(String message) {
        return failure(CODE_FAILURED, message);
    }


    /**
     * 处理失败
     *
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody failureApi(String message,Object object) {
        return failureApi(CODE_FAILURED, message,object);
    }


    /**
     * 处理失败，并返回数据（一般为错误信息）
     *
     * @param code
     *            错误代码
     * @param message
     *            消息
     * @return data
     */
    public static final MyResponseBody failureApi(int code, String message,Object object) {
        return new MyResponseBody(code, message,object);
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JsonResult [code=" + code + ", message=" + message + ", data="
                + data + "]";
    }


}