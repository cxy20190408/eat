package com.cncqs.sevice.impl;

import com.cncqs.User;
import com.cncqs.mapper.UserMapper;
import com.cncqs.sevice.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 3:27
 */
@Service
public class UserImpl implements IUser{
    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectOne(){
        return userMapper.selectOne();
    }

}