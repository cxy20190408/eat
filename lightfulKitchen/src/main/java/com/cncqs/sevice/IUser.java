package com.cncqs.sevice;

import com.cncqs.User;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 3:26
 */
public interface IUser {
    User selectOne();
}
