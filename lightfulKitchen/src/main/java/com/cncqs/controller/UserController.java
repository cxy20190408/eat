package com.cncqs.controller;

import com.cncqs.sevice.IUser;
import com.cncqs.utils.MyResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 3:24
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUser userImpl;

    @RequestMapping("selectOne")
    @ResponseBody
    public MyResponseBody selectOne() {
        MyResponseBody responseBody = MyResponseBody.success();
        responseBody.setData(userImpl.selectOne());
        return responseBody;
    }

}