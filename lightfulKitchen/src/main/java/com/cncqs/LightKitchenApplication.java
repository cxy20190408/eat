package com.cncqs;
/**
 * @author :cxy
 * 2019-4-8 15:11:12
 * */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LightKitchenApplication {

	public static void main(String[] args) {
		SpringApplication.run(LightKitchenApplication.class, args);
	}

}
