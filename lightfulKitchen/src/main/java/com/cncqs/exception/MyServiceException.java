package com.cncqs.exception;
/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 4:47
 */
public class MyServiceException extends RuntimeException {

    public static MyServiceException getInstance(String msg) {
        return new MyServiceException(msg);
    }

    public MyServiceException(String message) {
        super(message);
    }

}