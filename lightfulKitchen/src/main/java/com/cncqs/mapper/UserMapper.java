package com.cncqs.mapper;

import com.cncqs.User;
import org.springframework.stereotype.Repository;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 3:30
 */
@Repository
public interface UserMapper {
    User selectOne();
}
