package com.cncqs.common;

import java.lang.annotation.*;

/**
 * @author: cxy
 * @date: 2019/4/16 0016 上午 11:05
 * 权限验证注解
 */

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyValidate {

    String value() default "";
}