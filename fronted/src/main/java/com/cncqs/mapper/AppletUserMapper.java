package com.cncqs.mapper;

import com.cncqs.entity.AppletUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2019-04-10
 */
public interface AppletUserMapper extends BaseMapper<AppletUser> {

}
