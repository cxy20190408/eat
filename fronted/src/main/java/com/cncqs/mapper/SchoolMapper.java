package com.cncqs.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cncqs.entity.School;
import org.springframework.stereotype.Repository;

/**
 * @author: cxy
 * @date: 2019/4/9 0009 下午 3:17
 */
@Repository
public interface SchoolMapper extends BaseMapper<School>{

    School queryDetailsById(Long id);
}
