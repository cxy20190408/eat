package com.cncqs.mapper;

import com.cncqs.entity.LoginValidate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cxy
 * @since 2019-04-16
 */
public interface LoginValidateMapper extends BaseMapper<LoginValidate> {

}
