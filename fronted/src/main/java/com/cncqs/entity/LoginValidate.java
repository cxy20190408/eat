package com.cncqs.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.cncqs.base.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author cxy
 * @since 2019-04-16
 */
@TableName("login_validate")
public class LoginValidate extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("open_id")
    private String openId;

    private String token;

    /**
     * 上次登录时间
     */
    @TableField("login_time")
    private Date loginTime;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public static final String OPEN_ID = "open_id";

    public static final String TOKEN = "token";

    public static final String LOGIN_TIME = "login_time";

    @Override
    public String toString() {
        return "LoginValidate{" +
        "openId=" + openId +
        ", token=" + token +
        ", loginTime=" + loginTime +
        "}";
    }
}
