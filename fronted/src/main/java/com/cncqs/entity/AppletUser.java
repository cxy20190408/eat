package com.cncqs.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.cncqs.base.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author cxy
 * @since 2019-04-10
 */
@TableName("applet_user")
public class AppletUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 微信头像url
     */
    @TableField("avatar_url")
    private String avatarUrl;

    /**
     * 微信openId
     */
    @TableField("open_id")
    private String openId;

    private String remark;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static final String NICK_NAME = "nick_name";

    public static final String AVATAR_URL = "avatar_url";

    public static final String OPEN_ID = "open_id";

    public static final String REMARK = "remark";

    @Override
    public String toString() {
        return "AppletUser{" +
        "nickName=" + nickName +
        ", avatarUrl=" + avatarUrl +
        ", openId=" + openId +
        ", remark=" + remark +
        "}";
    }
}
