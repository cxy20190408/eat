package com.cncqs.service;

import com.cncqs.entity.LoginValidate;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.cncqs.entity.MyPage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cxy
 * @since 2019-04-16
 */
public interface ILoginValidateService extends IService<LoginValidate> {

    Page selectPage(LoginValidate loginValidate, MyPage<LoginValidate> page);


}
