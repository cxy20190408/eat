package com.cncqs.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cncqs.entity.AppletUser;
import com.cncqs.entity.MyPage;
import com.cncqs.mapper.AppletUserMapper;
import com.cncqs.service.IAppletUserService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cxy
 * @since 2019-04-10
 */
@Service
public class AppletUserServiceImpl extends ServiceImpl<AppletUserMapper, AppletUser> implements IAppletUserService {

    @Override
    public Page selectPage(AppletUser appletUser, MyPage<AppletUser> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(appletUser.getName()), AppletUser.NAME, appletUser.getName());
        return this.selectMapsPage(page, wrapper);
    }

}
