package com.cncqs.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cncqs.entity.LoginValidate;
import com.cncqs.entity.MyPage;
import com.cncqs.mapper.LoginValidateMapper;
import com.cncqs.service.ILoginValidateService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cxy
 * @since 2019-04-16
 */
@Service
public class LoginValidateServiceImpl extends ServiceImpl<LoginValidateMapper, LoginValidate> implements ILoginValidateService {

    @Override
    public Page selectPage(LoginValidate loginValidate, MyPage<LoginValidate> page) {
        Wrapper wrapper = new EntityWrapper();
        //wrapper.like(Strings.isNotEmpty(loginValidate.getName()), LoginValidate.NAME, loginValidate.getName());
        return this.selectMapsPage(page, wrapper);
    }

}
