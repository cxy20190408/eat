package com.cncqs.service.Impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cncqs.entity.School;
import com.cncqs.mapper.SchoolMapper;
import com.cncqs.service.ISchoolService;
import com.cncqs.utils.LatitudeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author: cxy
 * @date: 2019/4/9 0009 下午 3:23
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper,School> implements ISchoolService{
    @Autowired
    private SchoolMapper schoolMapper;

    @Override
    public School queryDetailsById(Long id) {
        return schoolMapper.queryDetailsById(id);
    }

    @Override
    public List<School> getSortListByLongitudeAndLatitude(Double lgt, Double lat) {
        Wrapper wrapper = new EntityWrapper<>();
        List<School> schools = this.selectList(wrapper);
        for (School school : schools) {
            double distance = LatitudeUtils.getDistance(lat, lgt, school.getLatitude(), school.getLongitude());
            school.setDistance(distance);
        }
        Collections.sort(schools);
        return schools;
    }
}