package com.cncqs.service;

import com.cncqs.entity.AppletUser;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.plugins.Page;
import com.cncqs.entity.MyPage;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cxy
 * @since 2019-04-10
 */
public interface IAppletUserService extends IService<AppletUser> {

    Page selectPage(AppletUser appletUser, MyPage<AppletUser> page);


}
