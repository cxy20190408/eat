package com.cncqs.service;

import com.baomidou.mybatisplus.service.IService;
import com.cncqs.entity.School;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author: cxy
 * @date: 2019/4/9 0009 下午 3:22
 */
public interface ISchoolService  extends IService<School>{

    School queryDetailsById(Long id );

    /**
     * 根据经纬度查询附近的学校
     * @param lgt 经度
     * @param lat 纬度
     * @return
     * */
    List<School> getSortListByLongitudeAndLatitude(Double lgt,Double lat);
}