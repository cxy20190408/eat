package com.cncqs.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: cxy
 * @date: 2019/4/12 0012 上午 11:40
 */
public class LatitudeUtils {

    public static final String KEY_1 = "7d9fbeb43e975cd1e9477a7e5d5e192a";
    private static double EARTH_RADIUS = 6378.137;

    /**
     * 根据地址算经纬度坐标
     * key lng(经度),lat(纬度)
     */
    public static Map<String,String> getGeocodingLatitude(String address){
        BufferedReader in = null;
        try {
            //将地址转换成utf-8的16进制
            address = URLEncoder.encode(address, "UTF-8");
            URL tirc = new URL("http://api.map.baidu.com/geocoder?address="+ address +"&output=json&key="+ KEY_1);


            in = new BufferedReader(new InputStreamReader(tirc.openStream(),"UTF-8"));
            String res;
            StringBuilder sb = new StringBuilder("");
            while((res = in.readLine())!=null){
                sb.append(res.trim());
            }
            String str = sb.toString();
            Map<String,String> map;
            if(StringUtils.isNotBlank(str)){
                int lngStart = str.indexOf("lng\":");
                int lngEnd = str.indexOf(",\"lat");
                int latEnd = str.indexOf("},\"precise");
                if(lngStart > 0 && lngEnd > 0 && latEnd > 0){
                    String lng = str.substring(lngStart+5, lngEnd);
                    String lat = str.substring(lngEnd+7, latEnd);
                    map = new HashMap<>(4);
                    map.put("lng", lng);
                    map.put("lat", lat);
                    return map;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 通过经纬度获取距离(单位：米)
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return 距离
     */
    public static double getDistance(double lat1, double lng1, double lat2,
                                     double lng2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000d) / 10000d;
        s = s * 1000;
        return s;
    }


    public static void main(String[] args){
        try {
            Map<String, String> address1 = LatitudeUtils.getGeocodingLatitude("合肥市新站区文忠路2600号");
            Map<String, String> address2 = LatitudeUtils.getGeocodingLatitude("合肥市兴园小区");
            double distance = getDistance(Double.parseDouble(address1.get("lat")), Double.parseDouble(address1.get("lng")),
                    Double.parseDouble(address2.get("lat")), Double.parseDouble(address2.get("lng")));

            System.out.println("两地相距" + distance/1000 + "公里");
        }catch (Exception e ){
            e.printStackTrace();
        }
    }


}