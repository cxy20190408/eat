package com.cncqs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class FrontedApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontedApplication.class, args);
	}

}
