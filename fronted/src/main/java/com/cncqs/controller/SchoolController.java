package com.cncqs.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.cncqs.base.MyResponseBody;
import com.cncqs.entity.School;
import com.cncqs.service.ISchoolService;
import com.cncqs.utils.LatitudeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author: cxy
 * @date: 2019/4/9 0009 下午 3:23
 */
@Controller
@RequestMapping("/school")
public class SchoolController {
    @Autowired
    private ISchoolService schoolService;
    private static Logger logger = LoggerFactory.getLogger(SchoolController.class);

    @RequestMapping("getSortList")
    @ResponseBody
    public MyResponseBody getOne(String address) {
        MyResponseBody responseBody = MyResponseBody.success();
        Map<String, String> geocodingLatitude = LatitudeUtils.getGeocodingLatitude(address);
        Double lat = Double.parseDouble(geocodingLatitude.get("lat"));
        Double lgt = Double.parseDouble(geocodingLatitude.get("lng"));
        List<School> sortListByLongitudeAndLatitude = schoolService.getSortListByLongitudeAndLatitude(lgt, lat);
        responseBody.setData(sortListByLongitudeAndLatitude);
        return responseBody;
    }
}