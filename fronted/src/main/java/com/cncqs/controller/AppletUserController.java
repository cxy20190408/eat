package com.cncqs.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.cncqs.base.MyResponseBody;
import com.cncqs.common.MyValidate;
import com.cncqs.entity.MyPage;
import com.cncqs.service.IAppletUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.cncqs.entity.AppletUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  controller
 * </p>
 *
 * @author cxy
 * @since 2019-04-10
 */
@Controller
@RequestMapping("/appletUser")
public class AppletUserController extends BaseController {

    @Autowired
    private IAppletUserService appletUserService;


    @GetMapping("list")
    @ResponseBody
    public Page list(AppletUser appletUser, MyPage<AppletUser> page) {
        return appletUserService.selectPage(appletUser, page);
    }

    @GetMapping
    public String index() {
        return "/appletUser/appletUser";
    }

    @MyValidate
    @RequestMapping("testValidate")
    @ResponseBody
    public MyResponseBody testValidate(){
        return MyResponseBody.success();
    }

}
