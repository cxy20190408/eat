package com.cncqs.config;

import com.baomidou.mybatisplus.MybatisConfiguration;
import com.baomidou.mybatisplus.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.mapper.LogicSqlInjector;
import com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.parser.ISqlParser;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: cxy
 * @date: 2019/4/8 0008 下午 4:08
 */
@Configuration
@MapperScan(basePackages = "com.cncqs.mapper", sqlSessionFactoryRef = "mybatisSqlSession")
public class MybatisPlusConfig {



    @Bean
    @Primary
    @ConfigurationProperties(prefix = "my.datasource")
    public HikariDataSource myDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean
    @Primary
    public SqlSessionFactory mybatisSqlSession() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(myDataSource());
        sqlSessionFactory.setTypeAliasesPackage("com.cncqs.entity");
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        //添加驼峰
        configuration.setMapUnderscoreToCamelCase(true);
        sqlSessionFactory.setConfiguration(configuration);
        sqlSessionFactory.setPlugins(new Interceptor[]{
                paginationInterceptor(),
//                new PerformanceInterceptor(),
                new OptimisticLockerInterceptor()
        });
        sqlSessionFactory.setGlobalConfig(globalConfiguration());

        return sqlSessionFactory.getObject();
    }

    @Bean
    @Primary
    public SqlSessionTemplate sqlSessionLocalTemplate() throws Exception {
        return new SqlSessionTemplate(mybatisSqlSession());
    }

    @Bean
    @Primary
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(myDataSource());
    }

    public GlobalConfiguration globalConfiguration() {
        GlobalConfiguration conf = new GlobalConfiguration(new LogicSqlInjector());
        conf.setIdType(2);
        conf.setMetaObjectHandler(new LMetaObjectHandler());
        return conf;
    }

    private PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setLocalPage(true);// 开启 PageHelper 的支持
        List<ISqlParser> sqlParserList = new ArrayList<>();
        // Object user = subject.getSession().getAttribute(USER_KEY);
//        TenantSqlParser tenantSqlParser = new MyTenantSqlParser();
//        tenantSqlParser.setTenantHandler(new TenantHandler() {
//            @Override
//            public Expression getTenantId() {
//                RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//                if (requestAttributes != null) {
//                    HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
//                    if (request != null) {
//                        Object obj = request.getSession().getAttribute("_tenant_id_");
//                        if (obj != null) {
//                            return new LongValue(obj.toString());
//                        }
//                    }
//                }
//                return new LongValue(1);
//            }
//
//            @Override
//            public String getTenantIdColumn() {
//                return "tenant_id";
//            }
//
//            @Override
//            public boolean doTableFilter(String tableName) {
//                String[] tables = filterTabs.split(",");
//                for (int i = 0; i<tables.length;i++) {
//                    if (tableName.equals(tables[i])) {
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//        sqlParserList.add(tenantSqlParser);
        paginationInterceptor.setSqlParserList(sqlParserList);
        return paginationInterceptor;
    }

}
